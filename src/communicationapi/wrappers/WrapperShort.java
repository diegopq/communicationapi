package communicationapi.wrappers;

/**
 * WrapperShort rappresenta l'implementazione della classe Wrapper
 * rispetto al tipo Short.
 */
public class WrapperShort extends Wrapper<Short> {

    public WrapperShort() {
        super("short");
    }

    @Override
    public void create(Object content) {
        if(!(content instanceof Short))
            return;

        this.content = Short.toString((Short) content);
        consistent = true;
    }

    @Override
    public Short unwrap() {
        return Short.parseShort(content);
    }

}