package communicationapi.packet;

/**
 * La classe Packet rappresenta una singola informazione
 * pronta per essere trasformata in JSON e trasferita.
 */
public class Packet {

    public String content;
    public String signature;

}
