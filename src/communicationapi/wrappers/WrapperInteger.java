package communicationapi.wrappers;

/**
 * WrapperInteger rappresenta l'implementazione della classe Wrapper
 * rispetto al tipo Integer.
 */
public class WrapperInteger extends Wrapper<Integer> {

    public WrapperInteger() {
        super("integer");
    }

    @Override
    public void create(Object content) {
        if(!(content instanceof Integer))
            return;

        this.content = Integer.toString((Integer) content);
        consistent = true;
    }

    @Override
    public Integer unwrap() {
        return Integer.parseInt(content);
    }

}