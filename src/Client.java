import java.net.*;
import java.io.*;
import communicationapi.CommunicationAPI;

public class Client {

    public static void main(String[] args) {
        String dest = "127.0.0.1";
        int port    = 6060;
        CommunicationAPI cmapi = new CommunicationAPI();

        try {
            cmapi.add("Prova", "string");
            cmapi.add(100, "integer");
            cmapi.add(true, "boolean");
            cmapi.add((byte) 0x01, "byte");
            cmapi.add('a', "character");
            cmapi.add((double) 100, "double");
            cmapi.add((float) 100, "float");
        }
        catch (IllegalArgumentException e) {
            System.out.println("<Error> " + e.getMessage());
        }

        try {
            System.out.println("> Connecting to " + dest + ":" + port);
            Socket cli = new Socket(dest, port);
            OutputStream toSrv = cli.getOutputStream();
            DataOutputStream out = new DataOutputStream(toSrv);

            System.out.println("> Sending the JSON string");
            out.writeUTF(cmapi.generateJSON());

            System.out.println("> Exiting");
        }
        catch(IOException e) {
            System.out.println("<Error> " + e.getMessage());
        }
    }

}
