package communicationapi;

import communicationapi.wrappers.Wrapper;
import java.util.HashMap;

/**
 * WrappersLoader si occupa di  caricare automaticamente
 * classi che estendono Wrapper che poi verranno utilizzate a
 * seconda del tipo di dato che dobbiamo analizzare.
 */
public class WrappersLoader {

    private HashMap<String, Class> wrappers = new HashMap<>();

    /**
     * addWrapper è un metodo utilizzato per aggiungere una
     * determinata classe che estende la classe Wrapper in modo
     * che sia poi utilizzabile a seconda delle necessità.
     *
     * @param name Il nome della classe che estende Wrapper.
     * @throws ClassNotFoundException
     */
    public void addWrapper(String name) throws ClassNotFoundException {
        Wrapper wrp = null;
        Class act   = Class.forName("communicationapi.wrappers." + name);

        try {
            wrp = (Wrapper) act.newInstance();
        }
        catch (ReflectiveOperationException e) {
            System.out.println("An error has occurred retrieving a specific Wrapper class: " + e.getMessage());
        }

        if(wrp != null)
            wrappers.put(wrp.getSignature(), act);
    }

    /**
     * getWrapper è il metodo utilizzato per ottenere il Wrapper specifico, tra
     * quelli caricati, che analizza una determinata signature.
     *
     * @param signature La signature che il Wrapper deve analizzare.
     * @return La classe specifica che implementa la classe Wrapper per la signature scelta.
     */
    public Wrapper getWrapper(String signature) {
        Wrapper wrp = null;
        Class act   = wrappers.get(signature);

        if(act == null)
            return null;

        try {
            wrp = (Wrapper) act.newInstance();
        }
        catch (ReflectiveOperationException e) {
            System.out.println("An error has occurred loading a specific Wrapper class: " + e.getMessage());
        }

        return wrp;
    }

}
