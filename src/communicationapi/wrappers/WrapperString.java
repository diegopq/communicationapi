package communicationapi.wrappers;

/**
 * WrapperString rappresenta l'implementazione della classe Wrapper
 * rispetto al tipo String.
 */
public class WrapperString extends Wrapper<String> {

    public WrapperString() {
        super("string");
    }

    @Override
    public void create(Object content) {
        if(!(content instanceof String))
            return;

        this.content = (String) content;
        consistent = true;
    }

    @Override
    public String unwrap() {
        return content;
    }

}
