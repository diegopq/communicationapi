package communicationapi;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.google.gson.reflect.TypeToken;
import communicationapi.packet.Packet;
import communicationapi.wrappers.Wrapper;

/**
 * Classe per l'implementazione del sistema di comunicazione
 * tramite JSON.
 */
public class CommunicationAPI {

    private List<Packet> inBuffer  = new ArrayList<>();
    private List<Packet> outBuffer = new ArrayList<>();

    private Gson gson;
    private WrappersLoader wl;

    public CommunicationAPI() {
        gson = new Gson();
        wl = new WrappersLoader();

        try {
            wl.addWrapper("WrapperString");
            wl.addWrapper("WrapperInteger");
            wl.addWrapper("WrapperBoolean");
            wl.addWrapper("WrapperByte");
            wl.addWrapper("WrapperCharacter");
            wl.addWrapper("WrapperDouble");
            wl.addWrapper("WrapperFloat");
            wl.addWrapper("WrapperLong");
            wl.addWrapper("WrapperShort");
        }
        catch (ClassNotFoundException e) {
            System.out.println("Specific class Wrapper not found: " + e.getMessage());
        }
    }

    /**
     * Metodo utilizzato per l'aggiunta di un frammento di informazione
     * alla coda che poi verrà trasformata in JSON.
     *
     * @param content L'informazione da trasferire.
     * @param signature Il tipo di informazione da trasferire.
     * @throws IllegalArgumentException
     */
    public void add(Object content, String signature) throws IllegalArgumentException {
        Wrapper wrp = wl.getWrapper(signature);

        if(wrp == null)
            throw new IllegalArgumentException("Signature \"" + signature + "\" not supported.");

        wrp.create(content);

        if(!wrp.isConsistent())
            throw new IllegalArgumentException("Inappropriate content for the signature \"" + signature + "\".");

        outBuffer.add(wrp.wrap());
    }

    /**
     * Il metodo che genera la String JSON.
     *
     * @return
     */
    public String generateJSON() {
        return gson.toJson(outBuffer);
    }

    /**
     * Il metodo che resetta il buffer di uscita.
     */
    public void clearOutBuffer() {
        outBuffer.clear();
    }

    /**
     * Metodo che parsa il contenuto della stringa JSON passata come
     * parametro.
     *
     * @param body La stringa JSON da analizzare.
     * @throws JsonSyntaxException
     */
    public void parseJSON(String body) throws JsonSyntaxException {
        Type packetList = new TypeToken<List<Packet>>(){}.getType();
        List<Packet> lst = gson.fromJson(body, packetList);

        for(Packet pkt : lst) {
            inBuffer.add(pkt);
        }
    }

    /**
     * Il metodo che ritorna la prossima informazione ottenuta dalla stringa JSON
     * analizzata precedentemente tramite parseJSON, sottoforma di una classe che
     * estende Wrapper.
     *
     * @return Il frammento di informazione sotto forma di classe Wrapper.
     * @throws RuntimeException
     */
    public Wrapper nextItem() throws RuntimeException {
        if(inBuffer.isEmpty())
            return null;

        Packet pkt  = inBuffer.remove(0);
        Wrapper wrp = wl.getWrapper(pkt.signature);

        if(wrp == null)
            throw new RuntimeException("Packet signature not supported.");

        wrp.create(pkt);
        return wrp;
    }

}
