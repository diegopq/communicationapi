package communicationapi.wrappers;

/**
 * WrapperDouble rappresenta l'implementazione della classe Wrapper
 * rispetto al tipo Double.
 */
public class WrapperDouble extends Wrapper<Double> {

    public WrapperDouble() {
        super("double");
    }

    @Override
    public void create(Object content) {
        if(!(content instanceof Double))
            return;

        this.content = Double.toString((Double) content);
        consistent = true;
    }

    @Override
    public Double unwrap() {
        return Double.parseDouble(content);
    }

}