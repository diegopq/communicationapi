import java.net.*;
import java.io.*;
import communicationapi.CommunicationAPI;
import communicationapi.wrappers.Wrapper;

public class Server {

    public static void main(String[] args) {
        int port = 6060;
        CommunicationAPI cmapi = new CommunicationAPI();

        try {
            ServerSocket serverSocket = new ServerSocket(port);

            System.out.println("> Waiting for connections");
            Socket srv = serverSocket.accept();

            System.out.println("> Accepted connection from " + srv.getRemoteSocketAddress());
            DataInputStream in = new DataInputStream(srv.getInputStream());

            cmapi.parseJSON(in.readUTF());

            Wrapper wrappedEl;
            Object unwrappedEl;
            while((wrappedEl = cmapi.nextItem()) != null) {
                unwrappedEl = wrappedEl.unwrap();

                if(unwrappedEl instanceof String)
                    System.out.println(unwrappedEl + " is a String.");
                else if(unwrappedEl instanceof Integer)
                    System.out.println(unwrappedEl.toString() + " is an Integer.");
                else if(unwrappedEl instanceof Boolean)
                    System.out.println(unwrappedEl.toString() + " is a Boolean.");
                else if(unwrappedEl instanceof Byte)
                    System.out.println(unwrappedEl.toString() + " is a Byte.");
                else if(unwrappedEl instanceof Character)
                    System.out.println(unwrappedEl.toString() + " is a Character.");
                else if(unwrappedEl instanceof Double)
                    System.out.println(unwrappedEl.toString() + " is a Double.");
                else if(unwrappedEl instanceof Float)
                    System.out.println(unwrappedEl.toString() + " is a Float.");
                else
                    System.out.println("Value not recognized");
            }
        }
        catch (IOException e) {
            System.out.println("<Error> " + e.getMessage());
        }

    }

}
