package communicationapi.wrappers;

import communicationapi.packet.Packet;

/**
 * Wrapper è una classe abstract che rappresenta un frammento di informazione
 * pronto per essere analizzato.
 *
 * @param <T>
 */
public abstract class Wrapper<T> {

    protected String signature;
    protected String content;

    protected boolean consistent;

    Wrapper(String signature) {
        this.signature  = signature;
        this.content    = "";
        this.consistent = false;
    }

    /**
     * Metodo che permette di controllare che la costruzione del Packet
     * e quindi anche della specifica classe Wrapper sia giusta.
     *
     * @return <i>True</i> se la classe è costruita correttamente.
     */
    public boolean isConsistent() {
        return consistent;
    }

    /**
     * Metodo che permette di ottenere la signature in questione.
     *
     * @return La signature.
     */
    public String getSignature() {
        return signature;
    }

    /**
     * Metodo utilizzato per costruire una istanza della classe Wrapper
     * da un singolo Packet, con la possibilità quindi di analizzarlo.
     *
     * @param pkt Il Packet da analizzare.
     */
    public void create(Packet pkt) {
        this.content = pkt.content;

        if(signature.equals(pkt.signature))
            consistent = true;
    }

    /**
     * Metodo utilizzato per restituire un Packet pronto per essere
     * trasmesso.
     *
     * @return L'oggetto Packet relativo.
     */
    public Packet wrap() {
        Packet pkt = new Packet();
        pkt.content = content;
        pkt.signature = signature;

        return pkt;
    }

    /**
     * Metodo utilizzato per restituire l'elemento analizzato dal pacchetto,
     * nel suo tipo corretto.
     *
     * @return L'elemento analizzato dal Packet.
     */
    public abstract T unwrap();

    /**
     * Metodo utilizzato per creare un'istanza della classe Wrapper
     * con un contenuto passato come parametro.
     *
     * @param content Il contenuto del Packet.
     */
    public abstract void create(Object content);

}
