package communicationapi.wrappers;

/**
 * WrapperBoolean rappresenta l'implementazione della classe Wrapper
 * rispetto al tipo Integer.
 */
public class WrapperBoolean extends Wrapper<Boolean> {

    public WrapperBoolean() {
        super("boolean");
    }

    @Override
    public void create(Object content) {
        if(!(content instanceof Boolean))
            return;

        this.content = Boolean.toString((Boolean) content);
        consistent = true;
    }

    @Override
    public Boolean unwrap() {
        return Boolean.parseBoolean(content);
    }

}