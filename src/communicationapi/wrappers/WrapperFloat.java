package communicationapi.wrappers;

/**
 * WrapperFloat rappresenta l'implementazione della classe Wrapper
 * rispetto al tipo Float.
 */
public class WrapperFloat extends Wrapper<Float> {

    public WrapperFloat() {
        super("float");
    }

    @Override
    public void create(Object content) {
        if(!(content instanceof Float))
            return;

        this.content = Float.toString((Float) content);
        consistent = true;
    }

    @Override
    public Float unwrap() {
        return Float.parseFloat(content);
    }

}