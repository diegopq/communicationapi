package communicationapi.wrappers;

/**
 * WrapperByte rappresenta l'implementazione della classe Wrapper
 * rispetto al tipo Byte.
 */
public class WrapperByte extends Wrapper<Byte> {

    public WrapperByte() {
        super("byte");
    }

    @Override
    public void create(Object content) {
        if(!(content instanceof Byte))
            return;

        this.content = Byte.toString((Byte) content);
        consistent = true;
    }

    @Override
    public Byte unwrap() {
        return Byte.parseByte(content);
    }

}