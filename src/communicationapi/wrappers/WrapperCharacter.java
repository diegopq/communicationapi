package communicationapi.wrappers;

/**
 * WrapperCharacter rappresenta l'implementazione della classe Wrapper
 * rispetto al tipo Character.
 */
public class WrapperCharacter extends Wrapper<Character> {

    public WrapperCharacter() {
        super("character");
    }

    @Override
    public void create(Object content) {
        if(!(content instanceof Character))
            return;

        this.content = Character.toString((Character) content);
        consistent = true;
    }

    @Override
    public Character unwrap() {
        return content.charAt(0);
    }

}