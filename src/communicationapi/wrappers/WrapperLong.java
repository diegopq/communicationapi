package communicationapi.wrappers;

/**
 * WrapperLong rappresenta l'implementazione della classe Wrapper
 * rispetto al tipo Long.
 */
public class WrapperLong extends Wrapper<Long> {

    public WrapperLong() {
        super("long");
    }

    @Override
    public void create(Object content) {
        if(!(content instanceof Long))
            return;

        this.content = Long.toString((Long) content);
        consistent = true;
    }

    @Override
    public Long unwrap() {
        return Long.parseLong(content);
    }

}